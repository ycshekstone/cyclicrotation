function solution(A, K) {

    if (A.length == K || K == 0) {
        return A;
    }

    K = K % A.length;

    for (let i = 0; i < K; i++) {
        A.unshift(A.pop());
    }

    return A;
}

let A = [3, 8, 9, 7, 6];
let K = 3;
console.log(solution(A, K));